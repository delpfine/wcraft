import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs/observable/of';
import {Observable} from 'rxjs/Observable';
import {User} from './user';

@Injectable()
export class UserService {
  private host = 'http://' + document.location.hostname + ':1111';

  constructor(private http: HttpClient) {
  }

  getUser(id: number): Observable<User> {
    const url = `${this.host}/users/${id}`;
    return this.http.get<User>(url).pipe(
      // tap(user => console.log(`fetched user: ${id}`)),
      catchError(this.handleError<User>(`getUser id=${id}`))
    );
  }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(`${this.host}/users`).pipe(
      // tap(users => console.log(`fetched users`)),
      catchError(this.handleError('getUsers', []))
    );
  }

  searchUsers(term: string): Observable<User[]> {
    return this.http.get<User[]>(`${this.host}/users?first_name_like=${term.trim()}`).pipe(
      // tap(_ => console.log(`found users matching "${term}"`)),
      catchError(this.handleError<User[]>('searchUsers', []))
    );
  }

  updateUser(user: User): Observable<any> {
    return this.http.put(`${this.host}/users/${user.id}`, user).pipe(
      // tap(_ => console.log(`updated user id=${user.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  createUser(user: User): Observable<User> {
    return this.http.post<User>(`${this.host}/users`, user).pipe(
      // tap((user: User) => console.log(`added user w/ id=${user.id}`)),
      catchError(this.handleError<User>('createUser'))
    );
  }

  deleteUser(id: number): Observable<User> {
    console.log(id);
    return this.http.delete<User>(`${this.host}/users/${id}`).pipe(
      // tap(_ => console.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
}
