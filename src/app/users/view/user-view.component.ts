import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';
import {User} from '../user';

declare var UIkit: any;
declare var Icon: any;

@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: ['../users.scss']
})
export class UserViewComponent implements OnInit {
  user: User;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService
  ) {
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id)
      .subscribe(data => this.user = data);
  }

  deleteUser(id): void {
    UIkit.modal.confirm('Are you sure that you want to delete the user?').then(() => {
      this.userService.deleteUser(id).subscribe(() => {
        UIkit.notification('User has been deleted!', 'success');
        this.goHome();
      });
    }, () => {
    });
  }

  goHome(): void {
    this.router.navigate(['']);
  }
}
