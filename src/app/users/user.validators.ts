import {Validators} from '@angular/forms';
import {User} from './user';

export class UserValidators {

  constructor(private user: User) {
  }

  avatar = [this.user.avatar,
    [
      Validators.required,
      Validators.pattern(/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}/)
    ]
  ];
  first_name = [this.user.first_name,
    [
      Validators.required,
      Validators.pattern(/[А-яA-z]/)
    ]
  ];
  last_name = [this.user.last_name,
    [
      Validators.required,
      Validators.pattern(/[А-яA-z]/)
    ]
  ];
}
