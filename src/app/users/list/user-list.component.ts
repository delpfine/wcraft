import {Component, OnInit, Input} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user.service';
import {UserValidators} from '../user.validators';
import {User} from '../user';

declare var UIkit: any;
declare var Icon: any;

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['../users.scss']
})
export class UserListComponent implements OnInit {
  @Input() user: User;
  userForm: FormGroup;
  users: User[];

  constructor(private http: HttpClient,
              private userService: UserService,
              private fb: FormBuilder) {
  }

  ngOnInit(): void {
    this.showUsers();
  }

  showUsers(): void {
    this.userService.getUsers()
      .subscribe(data => {
        this.users = data;
        this.initForm();
      });
  }

  searchUsers(q: string): void {
    this.userService.searchUsers(q)
      .subscribe(data => this.users = data);
  }

  /**
   * TODO: close modal after validation
   */
  saveUser(): any {
    if (!this.userForm.invalid) {
      this.userService.createUser(this.user).subscribe(newUser => this.users.push(newUser));
      UIkit.notification('User has been added!', 'success');
    }
    return false;
  }

  deleteUser(id: number): void {
    UIkit.modal.confirm('Are you sure that you want to delete the user?').then(() => {
      this.userService.deleteUser(id).subscribe(() => {
          UIkit.notification('User has been deleted!', 'success');
        }
      );
      this.users = this.users.filter(u => u.id !== id);
    }, () => {
    });
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.userForm.controls[controlName];
    return control.invalid && control.touched;
  }

  private initForm(): void {
    this.userForm = this.fb.group(new UserValidators(this.user = new User()));
  }
}
