import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../user.service';
import {FormBuilder, FormGroup} from '@angular/forms';
import {User} from '../user';
import {UserValidators} from '../user.validators';

declare var UIkit: any;

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['../users.scss']
})
export class UserEditComponent implements OnInit {

  @Input() user: User;
  userForm: FormGroup;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private userService: UserService,
              private fb: FormBuilder) {
  }

  ngOnInit() {
    this.getUser();
  }

  getUser(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.userService.getUser(id)
      .subscribe(data => {
        this.user = data;
        this.initForm();
      });
  }

  save(): void {
    if (!this.userForm.invalid) {
      this.userService.updateUser(this.user).subscribe(() => {
        UIkit.notification('User has been updated!', 'success');
         this.goHome();
      });
    }
  }

  goHome(): void {
    this.router.navigate(['']);
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.userForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  private initForm(): void {
    this.userForm = this.fb.group(new UserValidators(this.user));
  }
}
