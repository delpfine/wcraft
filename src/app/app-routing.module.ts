import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UserViewComponent} from './users/view/user-view.component';
import {UserListComponent} from './users/list/user-list.component';
import {UserEditComponent} from './users/edit/user-edit.component';

const routes: Routes = [
  {path: '', component: UserListComponent},
  {path: 'view/:id', component: UserViewComponent, pathMatch: 'full'},
  {path: 'edit/:id', component: UserEditComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})

export class AppRoutingModule {
}
